import 'package:feel_logger/chart.dart';
import 'package:feel_logger/data_storage.dart';
import 'package:flutter/material.dart';
import 'package:feel_logger/data_point.dart';

final dropDownKey = GlobalKey<DropdownButtonState>();
final List<String> list =
    List<String>.generate(10, (index) => (index + 1).toString());

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  database.open();
  runApp(MaterialApp(
    themeMode: ThemeMode.dark,
    theme: ThemeData.dark(),
    darkTheme: ThemeData.dark(),
    home: const FeelLoggerUI(),
    routes: {
      "/chart": (BuildContext context) => const Chart(),
    },
  ));
}

class FeelLoggerUI extends StatefulWidget {
  const FeelLoggerUI({super.key});

  @override
  State<FeelLoggerUI> createState() => _AppState();
}

class _AppState extends State<FeelLoggerUI> {
  String status = "";
  bool canSave = true;
  void dock() async {
    if (!canSave) return;
    final level = dropDownKey.currentState!.dropdownValue;
    saveFeelStatus(level);
    final counts = await database.counts();
    setState(() {
      canSave = false;
      status = "thank you, currently there is $counts data points stored";
    });
    Future.delayed(const Duration(seconds: 5), () {
      setState(() {
        canSave = true;
        status = "";
      });
    });
  }

  NavigatorState getNav(BuildContext context) {
    return Navigator.of(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        floatingActionButton: FloatingActionButton(
            child: Image.network(
                "https://cdn.iconscout.com/icon/free/png-512/free-chart-growth-1913955-1624750.png"),
            onPressed: () {
              getNav(context).pushNamed("/chart");
            }),
        appBar: AppBar(title: const Text('Feel Logger')),
        body: Column(
          children: [
            const Center(
              child: Text("On a scale from 1 to 10"),
            ),
            Row(children: [
              const Expanded(
                child: Text(""),
              ),
              const Center(child: Text("    How do you feel?    ")),
              Center(
                child: MyDropdownButton(key: dropDownKey),
              ),
              const Expanded(
                child: Text(""),
              ),
            ]),
            Center(
              child: ElevatedButton(
                  onPressed: canSave ? dock : null, child: const Text("Save")),
            ),
            Center(
              child: Text(status),
            ),
          ],
        ));
  }
}

class MyDropdownButton extends StatefulWidget {
  const MyDropdownButton({required Key key}) : super(key: key);

  @override
  State createState() => DropdownButtonState();
}

class DropdownButtonState extends State<MyDropdownButton> {
  String dropdownValue = list[(list.length / 2).floor() - 1];

  @override
  Widget build(BuildContext context) {
    return DropdownButton<String>(
      value: dropdownValue.toString(),
      icon: const Icon(Icons.arrow_downward),
      elevation: 16,
      style: const TextStyle(color: Colors.deepPurple),
      underline: Container(
        height: 2,
        color: Colors.deepPurpleAccent,
      ),
      onChanged: (String? value) {
        // This is called when the user selects an item.
        setState(() {
          dropdownValue = value!;
        });
      },
      items: list.map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Center(child: Text("  $value  ")),
        );
      }).toList(),
    );
  }
}
