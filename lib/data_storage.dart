import 'dart:io';
import 'dart:async';

import 'package:path/path.dart';
import 'package:sqflite_common_ffi/sqflite_ffi.dart';

final dpStorage = <DataPoint>[];
final database = DataPointProvider();
const tableName = "feels";
const tsColumn = "timestamp";
const valColumn = "value";

class DataPoint {
  late int level;
  DateTime instance = DateTime.now();

  DataPoint(this.level);

  Map<String, Object?> toMap() {
    return {
      valColumn: level,
      tsColumn: instance.toIso8601String(),
    };
  }

  DataPoint.fromMap(Map<String, Object?> map) {
    level = map[valColumn] as int;
    instance = DateTime.parse(map[tsColumn] as String);
  }

  @override
  String toString() {
    return "DataPoint{level:$level, instance:$instance}";
  }
}

class DataPointProvider {
  late Database _sqlite;

  Future<void> open() async {
    await _createDB();
  }

  Future<void> _createDB() async {
    if (Platform.isWindows || Platform.isLinux) {
      // Initialize FFI
      sqfliteFfiInit();
      databaseFactory = databaseFactoryFfi;
    }
    final basePath = await databaseFactory.getDatabasesPath();
    final dbPath = join(basePath, 'feel_logger.db');
    await Directory(basePath).create(recursive: true);
    _sqlite = await openDatabase(dbPath, version: 1,
        onCreate: (Database db, int version) async {
      await db.execute('''
        create table if not exists $tableName (
          $tsColumn text,
          $valColumn integer
        )
        ''');
    });
  }

  DataPointProvider() {
    open();
  }

  Future<DataPoint> insert(DataPoint dp) async {
    await _sqlite.insert(tableName, dp.toMap());
    return dp;
  }

  Future<List<DataPoint>> getRecent(DateTime tp) async {
    List<DataPoint> out = <DataPoint>[];
    var rows = await _sqlite.query(tableName,
        where: '$tsColumn > ?', whereArgs: [tp.toIso8601String()]);
    rows.every((element) {
      out.add(DataPoint.fromMap(element));
      return true;
    });
    return out;
  }

  Future<int> counts() async {
    return (await _sqlite.query(tableName)).length;
  }
}
