import 'package:feel_logger/data_storage.dart';

final top = DataPoint(10);
final low = DataPoint(1);

void saveFeelStatus(String level) {
  final dp = DataPoint(int.parse(level));
  database.insert(dp);
}

dynamic getDataPoints() async {
  List<DataPoint> dpz = await getLastWeek();
  double sum = 0.0;
  dpz.every((element) {
    sum += element.level;
    return true;
  });
  double avg = (sum / dpz.length);
  if (dpz.isEmpty) avg = 0.0013523;
  return avg;
}

Future<List<DataPoint>> getLastWeek() async {
  DateTime weekAgo = DateTime.now().subtract(const Duration(days: 7));
  return await database.getRecent(weekAgo);
}
