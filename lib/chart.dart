import 'package:feel_logger/data_point.dart';
import 'package:feel_logger/data_storage.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:format/format.dart';

class Chart extends StatefulWidget {
  const Chart({super.key});

  @override
  State<Chart> createState() => _ChartState();
}

class _ChartState extends State<Chart> {
  double avg = 0.0;
  List<DataPoint> plot = [];

  Widget dateTimePoints(double value, TitleMeta meta, List<DataPoint> plot) {
    int index = value.toInt();
    if (plot.isEmpty) return Container();
    if (index >= plot.length) return Container();
    if (index < 0) return Container();
    DateTime dt = plot[value.toInt()].instance;
    var date = '{}-{:02}-{:02}'.format(dt.year, dt.month, dt.day);
    var time = '{:02}:{:02}'.format(dt.hour, dt.minute);
    String rep = '$date\n$time';
    return RotatedBox(quarterTurns: -1, child: Text(rep));
  }

  Future<void> _getAvg() async {
    double newAvg = await getDataPoints();
    setState(() {
      avg = newAvg;
    });
  }

  Future<void> _getPlot() async {
    List<DataPoint> points = await getLastWeek();
    setState(() {
      plot = points;
    });
  }

  @override
  Widget build(BuildContext context) {
    AxisTitles noView = const AxisTitles(
      sideTitles: SideTitles(showTitles: false),
    );

    AxisTitles dt = AxisTitles(
      sideTitles: SideTitles(
          getTitlesWidget: (val, mt) => dateTimePoints(val, mt, plot),
          showTitles: true,
          interval: 1.0,
          reservedSize: 80),
    );
    return Scaffold(
      appBar: AppBar(title: const Text('Feel Logger')),
      body: Column(children: [
        const Center(child: Text("Feel Chart")),
        const Center(child: Text("chart")),
        Container(
          padding: const EdgeInsets.all(15),
          height: 250,
          width: double.infinity,
          child: FutureBuilder<void>(
              future: _getPlot(),
              builder: (BuildContext ctx, AsyncSnapshot sn) {
                return LineChart(LineChartData(
                    gridData: const FlGridData(show: false),
                    maxY: 11.0,
                    minY: 0.0,
                    minX: -1.0, // add a little extra air around the lines
                    maxX: (plot.length).toDouble(),
                    titlesData: FlTitlesData(
                      topTitles: noView,
                      bottomTitles: dt,
                      rightTitles: noView,
                    ),
                    lineBarsData: [
                      LineChartBarData(
                          isCurved: true,
                          color: Colors.deepPurple.shade500,
                          spots: plot
                              .asMap()
                              .entries
                              .map((e) => FlSpot(
                                  e.key.toDouble(), e.value.level.toDouble()))
                              .toList()),
                      LineChartBarData(
                          color: Colors.deepOrange.shade300,
                          dotData: const FlDotData(show: false),
                          spots: plot
                              .asMap()
                              .entries
                              .map((e) => FlSpot(e.key.toDouble(), avg))
                              .toList())
                    ]));
              }),
        ),
        Center(
          child: FutureBuilder<void>(
              future: _getAvg(),
              builder: (BuildContext ctx, AsyncSnapshot<void> sn) {
                return Row(
                  children: [
                    const Spacer(),
                    const Text("Average past week: "),
                    Text('{:1.5}'.format(avg)),
                    const Spacer(),
                  ],
                );
              }),
        )
      ]),
    );
  }
}
