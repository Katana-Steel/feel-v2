# Feel Logger

A simple mood tracker, where you can add you current mood on a scale from 1 - 10
where you get to decide which is better ;)

All points are stored and can generate a Line Graph and will also provide the average
of all data point over the past week.

## Getting Started

Build dependencies:
* [flutter](https://docs.flutter.dev/get-started/install)

```bash
flutter pub get
flutter build linux --release
build/linux/x64/release/bundle/feel_logger
```